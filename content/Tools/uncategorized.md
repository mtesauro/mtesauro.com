---
title: "Uncategorized"
date: 2019-06-04T11:36:56-05:00
draft: false
categories:
  - "Tools"
tags:
  - "Tools"
  - "TODO"
---

### Uncategorized tools I've stumpled upon

* https://fbinfer.com/
  * Java (Android), C, C++, and iOS/Objective-C
* https://blog.chromium.org/2014/04/testing-chromium-threadsanitizer-v2.html
  * Detect data races in C++ and Go code, also report synchronization issues like deadlocks, unjoined threads, destroying locked mutexes, use of async-signal unsafe code in signal handlers, and others.
  * https://github.com/google/sanitizers
* https://spotbugs.github.io/
  * Static analysis looking for bugs in Java code
