---
title: "Queue of pending items"
date: 2019-06-03T18:59:36-05:00
draft: true
---

### Uncategorized items

* https://nvd.nist.gov/vuln/search
* https://swtch.com/~rsc/regexp/regexp4.html
  * https://github.com/google/codesearch
* https://opensource.google.com/docs/thirdparty/oneversion/
* https://www.imperialviolet.org/2009/08/26/seccomp.html
  * https://chromium.googlesource.com/chromium/src/+/master/docs/design/sandbox.md
* Need to test this out with GASP containers
  * https://cloud.google.com/blog/products/gcp/open-sourcing-gvisor-a-sandboxed-container-runtime
* Golang and AppEngine: https://blog.golang.org/the-app-engine-sdk-and-workspaces-gopath
* https://securityboulevard.com/2019/05/20-best-continuous-integration-tools-a-guide-to-optimizing-your-ci-cd-processes/
* Installing debs within the confines of a directory that isn't /
  * https://askubuntu.com/questions/193695/installing-packages-into-local-directory
  * https://askubuntu.com/questions/339/how-can-i-install-a-package-without-root-access/350#350
* Look into OpenStack's Zuul as a master pipeline runner
  * https://zuul-ci.org/docs/zuul/
  * https://zuul-ci.org/
* Interesting Dockers and dockerfiles for reference
  * https://hub.docker.com/u/monachus
* R&D this: https://editorconfig.org/
  * Vim plugin for ^ https://github.com/editorconfig/editorconfig-vim#readme
* https://github.com/k0kubun/tetris => Golang terminal Tetris
* During research of Duo Golang course videos
  * https://dev.to/theodesp
  * https://dev.to/theodesp/a-step-by-step-guide-to-go-internationalization-i18n--localization-l10n-4ig2
  * https://github.com/theodesp/awesome-coding-camps
  * https://github.com/GoBootcamp/book
  * https://www.youtube.com/user/toddmcleod/playlists
  * https://www.udemy.com/course/learn-how-to-code/?couponCode=%20letsgo
* Good Golang http.Client advice
  * https://medium.com/@nate510/don-t-use-go-s-default-http-client-4804cb19f779
* Git cheats
  * git clone <repo_url> -b <branch_name> --single-branch
* https://tldrlegal.com/
* Look at this for MFA for Dojo perhaps: https://www.privacyidea.org/about/demo-site/
* Collaborative drawing: https://drawpile.net/
* Playstation 3 emulator: https://rpcs3.net/
* OpenAPI Spec: https://github.com/OAI/OpenAPI-Specification & https://www.openapis.org/
* OpenAPI Implementations: https://github.com/OAI/OpenAPI-Specification/blob/master/IMPLEMENTATIONS.md#implementations
* OpenAPI Mock Server: https://github.com/danielgtaylor/apisprout
* https://commonmark.org/
* https://www.nist.gov/itl/applied-cybersecurity/nice
* https://www.cybrary.it/
* https://www.electricmonk.nl/docs/dependency_resolving_algorithm/dependency_resolving_algorithm.html
* https://martinfowler.com/articles/is-quality-worth-cost.html
* https://martinfowler.com/bliki/StateOfDevOpsReport.html
* https://thehackernews.com/2019/07/linux-gnome-spyware.html
* https://blog.jessfraz.com/post/the-business-executives-guide-to-kubernetes/
* https://gitea.io/en-us/
* https://github.com/kgretzky/evilginx2
* https://github.com/bettercap/bettercap
* https://github.com/swisskyrepo/PayloadsAllTheThings
* https://github.com/danielmiessler/SecLists
* https://github.com/fuzzdb-project/fuzzdb
* https://github.com/mandatoryprogrammer/xsshunter
* https://github.com/wee-slack/wee-slack
* https://securityaffairs.co/wordpress/88948/hacking/libreoffice-flaw.html
* https://github.com/ubuntu/microk8s
*
