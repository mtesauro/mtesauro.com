---
title: "Golang Modules"
date: 2019-06-03T20:29:08-05:00
draft: false
categories:
  - "Golang"
tags:
  - "Golang"
  - "Modules"
---

### Learning about the new Golang Modules

* [Using Go Modules](https://blog.golang.org/using-go-modules)
  * A great overview and intro to go modules. Run through the examples to get a feel for how they work. Update to version go1.12.5 if needed to ensure output is similar to the provided examples.

### Reading queue

* Russ Cox on [Our Software Dependency Problem](https://research.swtch.com/deps)
* [Go's plans for modules](https://blog.golang.org/modules2019)

