---
title: "Graphql"
date: 2019-06-03T20:16:17-05:00
draft: false
categories:
  - "Golang"
  - "GraphQL"
tags:
  - "Golang"
  - "GraphQL"
---

To Read:

* [Golang and GraphQL](https://www.thepolyglotdeveloper.com/2018/05/getting-started-graphql-golang/)
* [Golang, GraphQL and NoSQL](https://blog.couchbase.com/using-graphql-with-golang-and-a-nosql-database/)

